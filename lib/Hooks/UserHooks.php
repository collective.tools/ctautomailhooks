<?php
namespace OCA\CTAutoMailHooks\Hooks;
use OCP\ILogger;
use OCP\IUserManager;
use OCA\CTAutoMailHooks\Config;
class UserHooks {
    private $logger;
    private $config;
    private $userManager;
    private $logContext = ['app' => 'ct_auto_mail_hooks'];
    public function __construct(
        ILogger $logger, Config $config, IUserManager $userManager) {
        $this->logger = $logger;
        $this->config = $config;
        $this->userManager = $userManager;
    }
    public function register() {
        $this->userManager->listen(
            '\OC\User', 'postCreateUser',$this->createUserCB());
        $this->userManager->listen(
            '\OC\User', 'preDelete', $this->deleteUserCB());
        $this->userManager->listen(
            '\OC\User', 'postSetPassword', $this->updateEmailPasswordCB());
    }
    private function createUserCB() {
        return function (\OC\User\User $user, string $password) {
            $newuser = array(
                'email' => $user->getUID() . $this->config->getEmailAddressSuffix(),
                'displayed_name' => $user->getDisplayName(),
                'raw_password' => $password,
                'quota_bytes' => $this->config->getEmailQuotaMB() * 1024 * 1024,
                'enabled' => true,
                'enable_imap' => true,
                'spam_enable' => true,
                'spam_mark_as_read' => true,
                'spam_threshold' => 80
            );

            if($this->createEmailAccount($newuser)) {
                $this->logger->warning(
                    "Automatically created mail account for uid " . $uid
                    . " with e-mail address \"" . $newuser["email"]
                    . " e-mail address suffix was \"". $email_suffix . "\"."
                    , $this->logContext);
            }
            else {
                $this->logger->error(
                    "Error creating mail account OR mail folders for uid "
                    . $uid);
            }
        };
    }

    private function deleteUserCB() {
        return function (\OC\User\User $user) {
            $email = $user->getUID() . $this->config->getEmailAddressSuffix();
            if($this->deleteEmailAccount($email)) {
                $this->logger->warning("Deleted mail account: " .  $email, $this->logContext);
            } else {
                $this->logger->error("Error deleting mail account" . $email);
            }
        };
    }

    private function updateEmailPasswordCB() {
        return function (\OC\User\User $user, string $password) {
            $email = $user->getUID() . $this->config->getEmailAddressSuffix();
            if($this->updateEmailPassword($email, $password)) {
                $this->logger->warning("updated email password for account" .  $email, $this->logContext);
            } else {
                $this->logger->error("Error updating mail password for account" . $email);
            }
        };
    }

    private function createEmailAccount($user_data) {
        $ch = curl_init($this->config->getMailUAPI() . 'user');
        $payload = json_encode($user_data);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $payload );
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json',
                                                   'Authorization:' . $this->config->getMailUKeys()));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true );
        $result = curl_exec($ch);

        if (curl_errno($ch)) {
            $message = 'createEmailAccount Error:' . curl_error($ch);
            $this->logger->error($message);
            return false;
        }
        $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        if ($code == 200) {
            return true;
        }

        $this->logger->error("createEmailAccount returned ".$code." message: ".$result);
        return false;
    }

    private function deleteEmailAccount($user) {
        $ch = curl_init($this->config->getMailUAPI() . 'user/' . urlencode($user));
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Authorization: ' . $this->config->getMailUKeys()));

        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true );
        $result = curl_exec($ch);

        if (curl_errno($ch)) {
            $message = 'deleteEmailAccount Error:' . curl_error($ch);
            $this->logger->error($message);
            return false;
        }
        $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        if ($code == 200) {
            return true;
        }

        $this->logger->error("deleteEmailAccount returned ".$code." message: ".$result);
        return false;
    }

    private function updateEmailPassword($user, $password) {
        $ch = curl_init($this->config->getMailUAPI() . 'user/' . urlencode($user));
        $payload = json_encode(array("raw_password" => $password));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $payload );
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json',
                                                   'Authorization:' . $this->config->getMailUKeys()));

        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PATCH");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true );
        $result = curl_exec($ch);

        if (curl_errno($ch)) {
            $message = 'updateEmailPassword Error:' . curl_error($ch);
            $this->logger->error($message);
            return false;
        }
        $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        if ($code == 200) {
            return true;
        }

        $this->logger->error("updateEmailPassword returned ".$code." message: ".$result);
        return false;
    }
}
