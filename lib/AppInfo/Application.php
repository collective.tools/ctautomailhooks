<?php
namespace OCA\CTAutoMailHooks\AppInfo;
use \OCP\AppFramework\App;
use \OCA\CTAutoMailHooks\Hooks\UserHooks;
class Application extends App {
  public function __construct(array $urlParams = array()) {
    parent::__construct('auto_mail_accounts', $urlParams);
    $container = $this->getContainer();
    $container->registerService('UserHooks', function($c) {
      return new UserHooks(
        $c->query('ServerContainer')->getLogger(),
        $c->query('OCA\CTAutoMailHooks\Config'),
        $c->query('ServerContainer')->getUserManager()
      );
    });
  }
}
