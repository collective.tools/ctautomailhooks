<?php
namespace OCA\CTAutoMailHooks;
use OCP\ILogger;
use \OCP\IConfig;
class Config {
  const CONFIG_KEY = 'ct_auto_mail_hooks';
  const CONFIG_KEY_MAILU_KEYS="mailu_keys";
  const CONFIG_KEY_MAILU_API="mailu_api";
  const CONFIG_KEY_EMAIL_ADDR_SUFFIX="email_suffix";
  const CONFIG_KEY_EMAIL_ADDR_QUOTA_MB="quota";

  const DEFAULT_EMAIL_ADDR_QUOTA_MB="200";
  private $logger;
  private $appConfiguration;
  private $logContext = ['app' => 'ct_auto_mail_hooks'];
  public function __construct(
      ILogger $logger, IConfig $nextCloudConfiguration) {
    $this->logger = $logger;
    $this->appConfiguration =
        $nextCloudConfiguration->getSystemValue(self::CONFIG_KEY);
  }
  public function getMailUKeys() {
      return $this->getConfValue(self::CONFIG_KEY_MAILU_KEYS);
  }
  public function getMailUAPI() {
      return $this->getConfValue(self::CONFIG_KEY_MAILU_API);
  }
  public function getEmailAddressSuffix() {
      return $this->getConfValue(self::CONFIG_KEY_EMAIL_ADDR_SUFFIX);
  }
  public function getEmailQuotaMB() {
      $quota = $this->getConfValue(self::CONFIG_KEY_EMAIL_ADDR_QUOTA_MB);
      return is_null($quota) ?  self::DEFAULT_EMAIL_ADDR_QUOTA_MB : $quota;
  }
  private function getConfValue($configKey) {
      if(array_key_exists($configKey, $this->appConfiguration)){
        return $this->appConfiguration[$configKey];
      }
      return NULL;
  }
}
